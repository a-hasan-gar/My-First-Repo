#include <bits/stdc++.h>

#define FAST_IO ios_base::sync_with_stdio(0); \
				cin.tie(0); \
				cout.tie(0)
#define pb push_back
#define fi first
#define se second
#define mp make_pair
#define all(_v)				_v.begin(), _v.end()
#define sz(_v)				(int) _v.size()
#define FIND(_obj, _val)	(_obj.find(_val) != _obj.end())
#define RESET(_a, _v)		fill_n(_a,sizeof(_a)/sizeof(_a[0]),_v)
#define REP(_i, _n)			for (int _i = 0; _i < (int) _n; _i++)
#define FOR(_i, _a, _b)		for (int _i = (int) _a; _i <= (int) _b; _i++)
#define FORD(_i, _a, _b)	for (int _i = (int) _a; _i >= (int) _b; _i--)
#define FORIT(_it, _obj)	for (auto _it = _obj.begin(); _it != _obj.end(); _it++)

// DEBUG UTIL
#define DEBUG(x) cerr << "> " << #x << " = " << x << endl

using namespace std;

using ll = long long;
using pii = pair<int,int>;
using pll = pair<ll,ll>;
using pdd = pair<double,double>;
using vi = vector<int>;
using vii = vector<pii>;
using vs = vector<string>;

const double PI = acos(-1.0);
const double EPS = 1e-14;
const int MOD = 1e9 + 7;
const int INF = 1e9;
const ll INFLL = 4e18;
const int MAX = 200;

struct Circle
{
	double x, y, r;
};

Circle a, b;

void read() {
	cin >> a.x >> a.y >> a.r;
	cin >> b.x >> b.y >> b.r;
}

bool is_inside(Circle c, double x, double y) {
	double rad = (c.x - x)*(c.x - x) + (c.y - y)*(c.y - y);

	return rad <= c.r*c.r;
}

void solve() {
	// Circle tmp; tmp.x = b.x, tmp.y = b.y, tmp.r = b.r;
	// if (a.r > b.r) {
	// 	b = a;
	// 	a = tmp;
	// }
	double x_l = min(a.x, b.x), x_u = max(a.x, b.x);
	double y_l = min(a.y, b.y), y_u = max(a.y, b.y);

	REP(loop,100) {
		double x_m = (x_l + x_u)/2.0;
		double y_m = (y_l + y_u)/2.0;

		if (is_inside(a,x_m,y_m) && is_inside(b,x_m,y_m)) break;
		else if (is_inside(a,x_m,y_m)) {
			if (a.x < b.x) x_l = x_m;
			else x_u = x_m;

			if (a.y < b.y) y_l = y_m;
			else y_u = y_m;
		}
		else {
			if (a.x < b.x) x_u = x_m;
			else x_l = x_m;

			if (a.y < b.y) y_u = y_m;
			else y_l = y_m;
		}
	}

	cout << fixed << setprecision(10);
	cout << (x_l + x_u)/2.0 << " " << (y_l + y_u)/2.0 << "\n";
}

int main() {
	FAST_IO;
	int TC = 1;
	while (TC--) {
		read();
		solve();
	}
} 