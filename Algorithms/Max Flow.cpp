#include <bits/stdc++.h>

#define FAST_IO ios_base::sync_with_stdio(0); \
				cin.tie(0); \
				cout.tie(0)
#define pb push_back
#define fi first
#define se second
#define mp make_pair
#define all(_v)				_v.begin(), _v.end()
#define sz(_v)				(int) _v.size()
#define FIND(_obj, _val)	(_obj.find(_val) != _obj.end())
#define RESET(_a, _v)		fill_n(_a,sizeof(_a)/sizeof(_a[0]),_v)
#define REP(_i, _n)			for (int _i = 0; _i < (int) _n; _i++)
#define FOR(_i, _a, _b)		for (int _i = (int) _a; _i <= (int) _b; _i++)
#define FORD(_i, _a, _b)	for (int _i = (int) _a; _i >= (int) _b; _i--)
#define FORIT(_it, _obj)	for (auto _it = _obj.begin(); _it != _obj.end(); _it++)

// DEBUG UTIL
#define DEBUG(x) cerr << "> " << #x << " = " << x << endl

using namespace std;

using ll = long long;
using ii = pair<int,int>;
using pll = pair<ll,ll>;
using pdd = pair<double,double>;
using vi = vector<int>;
using vii = vector<ii>;
using vs = vector<string>;

const double PI = acos(-1.0);
const double EPS = 1e-14;
const int MOD = 1e9 + 7;
const int INF = 1e9;
const ll INFLL = 4e18;
const int MAX = 100;

int TC, N, S, T, C;
int adj[MAX+5][MAX+5];
int mf;

void init() {
	mf = 0;
	memset(adj, 0, sizeof adj);
}

void read() {
	cin >> S >> T >> C;
	REP(i, C) {
		int u, v, c;
		cin >> u >> v >> c;
		adj[u][v] += c;
		adj[v][u] += c;
	}
}

int bfs() {
	bool vis[N+5];
	int from[N+5];
	memset(vis, false, sizeof vis);
	memset(from, -1, sizeof from);

	queue<int> que;
	que.push(S);
	vis[S] = true;
	while (!que.empty()) {
		int cur = que.front(); que.pop();
	}
}

void solve() {
	while (true) {
		int cap = bfs();
		if (cap == 0) break;
		else mf += cap;
	}
}

void print() {
	cout << "Network " << ++TC << "\n";
	cout << "The bandwith is " << mf << ".\n\n";
}

int main()
{
	FAST_IO;
	while (cin >> N) {
		init();
		read();
		solve();
		print();
	}
} 